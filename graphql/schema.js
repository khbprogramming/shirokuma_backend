const { gql } = require("apollo-server-express");

module.exports = gql`
  type User {
    _id: ID!
    userName: String!
    password: String!
    lastName: String
    firstName: String
    token: String
  }
  type Token {
    succes: String
    token: String
  }
  # type Post {
  #   _id: ID!
  #   name: String!
  #   slug: String!
  #   content: String
  # }

  type Query {
    seeUsers: [User]
    # posts: [Post]
    # onePost(id: ID!): Post
    me: User
  }

  type Mutation {
    createUser(userName: String!, password: String!): User
    login(userName: String!, password: String!): Token
    # addPost(name: String, slug: String, content: String): Post
    # updatePost(id: ID!, name: String, slug: String, content: String): Post
  }
`;
