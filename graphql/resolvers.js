const mongoose = require("mongoose");
const { UserInputError } = require("apollo-server");

const User = mongoose.model(
  "User",
  mongoose.Schema({
    userName: { type: String, required: true },
    password: { type: String, required: true },
    lastName: String,
    firstName: String,
    token: String,
  }),
  "users"
);

module.exports = {
  Query: {
    seeUsers: async (parent, args, context, info) => {
      const User = mongoose.model("User");
      const users = await User.find();
      return users;
    },
    me: async (parent, args, context, info) => {
      const User = mongoose.model("User");
      const user = await user.findOne({ userName: context.userName });
      console.log(context);
      if (!user) {
        return null;
      }
      return user;
    },
  },
  Mutation: {
    createUser: async (parent, args, context, info) => {
      const User = mongoose.model("User");
      const duplicate = await User.findOne({ userName: args.userName });
      if (duplicate) {
        throw new UserInputError("Хэрэглэгч бүртгэлтэй.");
      }
      const newUser = await new User(args).save();
      return newUser;
    },
    login: async (parent, args, context, info) => {
      const User = mongoose.model(User);
      const user = await User.findOne({ userName: args.userName });
      if (user.password === args.password) {
        return;
      }
    },
  },
};
