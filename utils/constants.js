const DEV = process.env.NODE_ENV !== "production";

module.exports = {
  DEV: DEV,
  JWT_SECRET: "secret",
};
