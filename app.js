const express = require("express");
const app = express();
const port = 8081;
const { DEV } = require("./utils/constants");
const mongoose = require(`mongoose`);
mongoose.connect(
  `mongodb+srv://nagisa:junichirou@cluster0-fjjt5.mongodb.net/shirokuma?retryWrites=true&w=majority`,
  {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  },
  (err) => {
    if (!err) {
      console.log("----database connected----");
    }
  }
);
if (DEV) {
  mongoose.set("debug", true);
}
const { ApolloServer } = require("apollo-server-express");
const typeDefs = require("./graphql/schema");
const resolvers = require("./graphql/resolvers");
const server = new ApolloServer({
  typeDefs,
  resolvers,
});

server.applyMiddleware({ app });
app.listen(port, () =>
  console.log(
    `//////////////////////////////////////////////////////////////////////////////////
Server start time: ${new Date()}
working at http://localhost:${port}
//////////////////////////////////////////////////////////////////////////////////`
  )
);
